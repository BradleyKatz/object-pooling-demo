# object-pooling-demo
Open the included SampleScene in order to run the project.

Most systems can be found/tweaked under the Systems GameObject in the scene (Cube spawning/pooling, Cube wandering, Neighbour detection).

The SpawnZone script controls where cubes will be spawned at runtime, it can be found/tweaked under Environment/Ground.

Note: The project was implemented in Unity 2021.1.19f1
