using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class EntitySpawner : MonoBehaviour
{
    [Header("UI References")]
    [SerializeField] private TMP_InputField spawnAmountField;
    [SerializeField] private TextMeshProUGUI spawnCountLabel;

    [Space]

    [Header("Spawn Zone")]
    [SerializeField] private SpawnZone spawnZone;

    [Space]

    [Header("Pooling")]
    [SerializeField] private GameObject prefabToSpawn;
    [SerializeField] private int startSize;
    [SerializeField] private int growthRate;

    [Space]

    [HideInInspector] public List<Transform> activeTransforms = new List<Transform>();

    [HideInInspector] public UnityEvent OnEntityCountChanged;

    private ObjectPool<GameObject> entitiesPool;
    private int numSpawnedEntities = 0;

    private void Awake()
    {
        entitiesPool = new ObjectPool<GameObject>
        (
            createFunc: InstantiateNewEntity,
            onGetObject: OnEntityRetrievedFromPool,
            onReturnedToPool: OnEntityReturnedToPool,
            onObjectDestroyed: OnEntityDestroyed,
            startSize: startSize,
            growthRate: growthRate
        );
    }

    public void Spawn()
    {
        if (int.TryParse(spawnAmountField.text, out int spawnAmount))
        {
            spawnAmount = Mathf.Abs(spawnAmount);

            numSpawnedEntities += spawnAmount;
            spawnCountLabel.text = numSpawnedEntities.ToString();

            for (int i = 0; i < spawnAmount; ++i)
            {
                _ = entitiesPool.Get();
            }

            OnEntityCountChanged?.Invoke();
        }
    }

    public void Despawn()
    {
        if (int.TryParse(spawnAmountField.text, out int despawnAmount))
        {
            despawnAmount = Mathf.Abs(despawnAmount);

            int delta = numSpawnedEntities - despawnAmount;
            numSpawnedEntities = delta >= 0 ? delta : 0;

            spawnCountLabel.text = numSpawnedEntities.ToString();

            for (int i = 0; i < despawnAmount; ++i)
            {
                for (int j = 0; j < entitiesPool.Count; ++j)
                {
                    if (entitiesPool[j].activeSelf)
                    {
                        entitiesPool.Return(entitiesPool[j]);
                        break;
                    }
                }
            }

            OnEntityCountChanged?.Invoke();
        }
    }

    #region Pooling Callbacks
    private GameObject InstantiateNewEntity()
    {
        var newObj = Instantiate(prefabToSpawn);
        newObj.SetActive(false);

        return newObj;
    }

    private void OnEntityRetrievedFromPool(GameObject entity)
    {
        activeTransforms.Add(entity.transform);

        entity.SetActive(true);
        entity.transform.position = spawnZone.GetRandomPosition();
        entity.transform.rotation = Quaternion.Euler(0f, Random.Range(-360f, 360f), 0f);
    }

    private void OnEntityReturnedToPool(GameObject entity)
    {
        activeTransforms.Remove(entity.transform);
        entity.SetActive(false);
    }

    private void OnEntityDestroyed(GameObject entity)
    {
        activeTransforms.Remove(entity.transform);
        Destroy(entity.gameObject);
    }
    #endregion
}
