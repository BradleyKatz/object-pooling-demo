using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZone : MonoBehaviour
{
    [SerializeField]
    private float x;

    [SerializeField]
    private float z;

    public Vector3 Center => transform.position;

    public Vector3 Min
    {
        get
        {
            float xMin = transform.position.x - x * 0.5f;
            float zMin = transform.position.z - z * 0.5f;

            return new Vector3(xMin, 0f, zMin);
        }
    }

    public Vector3 Max
    {
        get
        {
            float xMax = transform.position.x + x * 0.5f;
            float zMax = transform.position.z + z * 0.5f;

            return new Vector3(xMax, 0f, zMax);
        }
    }

    #region Clamp Within Zone
    public Vector3 ClampWithinZone(Vector3 inVector)
    {
        return new Vector3(Mathf.Clamp(inVector.x, Min.x, Max.x), inVector.y, Mathf.Clamp(inVector.z, Min.z, Max.z));
    }
    #endregion

    #region Get Random Position
    public Vector3 GetRandomPosition()
    {
        return new Vector3(Random.Range(Min.x, Max.x), 0f, Random.Range(Min.z, Max.z));
    }
    #endregion

    #region Draw Gizmos
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(transform.position, new Vector3(x, 0f, z));
    }
    #endregion
}
