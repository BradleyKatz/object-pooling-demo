using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using System;
using Unity.Collections.LowLevel.Unsafe;

public class NeighbourSystem : MonoBehaviour
{
    [Header("System References")]
    [SerializeField] private EntitySpawner spawner;

    [Space]

    [Header("Neighbour Line Drawing")]
    [SerializeField] private Material lineMaterial;

    private NativeArray<float3> allNeighbourPositions;

    private List<ComputeNeighboursJob> neighbourJobs;
    private List<Tuple<float3, float3>> neighbourPointsRenderBuffer;

    private void Awake()
    {
        spawner.OnEntityCountChanged.AddListener(UpdateNeighhbourSets);
        Camera.onPostRender += RenderNeighbourLines;
    }

    private void OnDestroy()
    {
        Camera.onPostRender -= RenderNeighbourLines;
        spawner.OnEntityCountChanged.RemoveListener(UpdateNeighhbourSets);
        CleanupNativeArrays();
    }

    private void Update()
    {
        if (spawner.activeTransforms.Count > 0)
        {
            UpdateNeighbourJobs();
        }
    }

    private void CleanupNativeArrays()
    {
        if (allNeighbourPositions.IsCreated)
            allNeighbourPositions.Dispose();
    }

    private void UpdateNeighhbourSets()
    {
        if (neighbourJobs == null)
        {
            neighbourJobs = new List<ComputeNeighboursJob>(spawner.activeTransforms.Count);
        }
        else
        {
            neighbourJobs.Clear();

            if (spawner.activeTransforms.Count > neighbourJobs.Capacity)
                neighbourJobs.Capacity = spawner.activeTransforms.Count;
        }

        if (neighbourPointsRenderBuffer == null)
        {
            neighbourPointsRenderBuffer = new List<Tuple<float3, float3>>(spawner.activeTransforms.Count);
        }
        else
        {
            neighbourPointsRenderBuffer.Clear();

            if (spawner.activeTransforms.Count > neighbourPointsRenderBuffer.Capacity)
                neighbourPointsRenderBuffer.Capacity = spawner.activeTransforms.Count;
        }

        CleanupNativeArrays();

        allNeighbourPositions = new NativeArray<float3>(spawner.activeTransforms.Count, Allocator.Persistent);
    }

    private void UpdateNeighbourJobs()
    {
        neighbourJobs?.Clear();

        var neighbourJobHandles = new NativeArray<JobHandle>(spawner.activeTransforms.Count, Allocator.TempJob);

        for (int i = 0; i < spawner.activeTransforms.Count - 1; ++i)
        {
            allNeighbourPositions[i] = spawner.activeTransforms[i].position;

            var job = new ComputeNeighboursJob();
            job.source = spawner.activeTransforms[i].position;
            job.closestNeighbour = new NativeArray<float3>(1, Allocator.TempJob);
            job.neighbourPositions = new NativeSlice<float3>(allNeighbourPositions, i + 1);

            job.closestNeighbour[0] = job.neighbourPositions[0];

            neighbourJobHandles[i] = job.Schedule(job.neighbourPositions.Length, 100);
            neighbourJobs.Add(job);
        }

        JobHandle.ScheduleBatchedJobs();
        JobHandle.CompleteAll(neighbourJobHandles);

        neighbourJobHandles.Dispose();

        neighbourPointsRenderBuffer.Clear();

        for (int i = 0; i < neighbourJobs.Count; ++i)
        {
            var currentJob = neighbourJobs[i];
            float3 p1 = currentJob.source;
            float3 p2 = currentJob.closestNeighbour[0];

            currentJob.closestNeighbour.Dispose();
            neighbourPointsRenderBuffer.Add(new Tuple<float3, float3>(p1, p2));
        }
    }

    private void RenderNeighbourLines(Camera camera)
    {
        if (neighbourPointsRenderBuffer == null || neighbourPointsRenderBuffer.Count == 0) return;

        lineMaterial?.SetPass(0);

        GL.PushMatrix();
        GL.MultMatrix(transform.localToWorldMatrix);
        GL.Begin(GL.LINES);

        for (int i = 0; i < neighbourPointsRenderBuffer.Count; ++i)
        {
            var currentLineTuple = neighbourPointsRenderBuffer[i];
            GlDrawLine(currentLineTuple.Item1, currentLineTuple.Item2);
        }

        GL.End();
        GL.PopMatrix();
    }

    private void GlDrawLine(float3 p1, float3 p2)
    {
        GL.Vertex(p1);
        GL.Vertex(p2);
    }

    #region Job Structs
    [BurstCompile]
    private struct ComputeNeighboursJob : IJobParallelFor
    {
        [ReadOnly]
        public float3 source;

        [ReadOnly, NativeDisableContainerSafetyRestriction]
        public NativeSlice<float3> neighbourPositions;

        [NativeDisableParallelForRestriction]
        public NativeArray<float3> closestNeighbour;

        public void Execute(int index)
        {
            float3 currentNeighbour = neighbourPositions[index];

            float currentNeighbourDist = math.distancesq(source, currentNeighbour);
            float closestNeighbourDist = math.distancesq(source, closestNeighbour[0]);

            if (currentNeighbourDist < closestNeighbourDist)
                closestNeighbour[0] = currentNeighbour;
        }
    }
    #endregion
}
