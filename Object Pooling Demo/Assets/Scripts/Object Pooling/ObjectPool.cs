using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> : IEnumerable where T : class
{
    #region Helper Class - PooledObjectRef
    private class PooledObjectRef
    {
        public bool inUse;
        public T objReference;

        public PooledObjectRef(T objReference)
        {
            inUse = false;
            this.objReference = objReference;
        }
    }
    #endregion

    private Func<T> createFunc;
    private Action<T> onGetObject;
    private Action<T> onReturnedToPool;
    private Action<T> onObjectDestroyed;

    private int growthRate;

    private List<T> pooledObjects;
    private LinkedList<PooledObjectRef> nextObjectLookup;

    public T this[int index] => pooledObjects[index];
    public int Count => pooledObjects.Count;

    #region Constructor
    public ObjectPool(
        Func<T> createFunc,
        Action<T> onGetObject,
        Action<T> onReturnedToPool,
        Action<T> onObjectDestroyed,
        int startSize,
        int growthRate)
    {
        this.createFunc = createFunc;
        this.onGetObject = onGetObject;
        this.onReturnedToPool = onReturnedToPool;
        this.onObjectDestroyed = onObjectDestroyed;
        this.growthRate = growthRate;

        nextObjectLookup = new LinkedList<PooledObjectRef>();
        Grow(startSize);
    }
    #endregion

    #region Grow - Instantiate more pooled elements
    private void Grow(int growthAmount)
    {
        if (pooledObjects == null)
            pooledObjects = new List<T>(growthAmount);
        else if (pooledObjects.Capacity <= pooledObjects.Count + growthAmount)
            pooledObjects.Capacity += growthAmount;

        for (int i = 0; i < growthAmount; ++i)
        {
            T obj = createFunc.Invoke();
            pooledObjects.Add(obj);

            var lookupNode = new LinkedListNode<PooledObjectRef>(new PooledObjectRef(obj));
            nextObjectLookup.AddFirst(lookupNode);
        }
    }
    #endregion

    #region Get
    public T Get()
    {
        var firstNode = nextObjectLookup?.First;

        // If the node at the front of the next object lookup
        // is in use, then we must need to grow our pool.
        if (firstNode == null || firstNode.Value.inUse)
            Grow(growthRate);

        firstNode = nextObjectLookup.First;
        var pooledObjectRef = firstNode.Value;
        pooledObjectRef.inUse = true;

        // Move the most recently referenced node to the end of list,
        // leaving the next (hopefully) unused node at the front.
        nextObjectLookup.RemoveFirst();
        nextObjectLookup.AddLast(firstNode);

        onGetObject?.Invoke(pooledObjectRef.objReference);
        return pooledObjectRef.objReference;
    }
    #endregion

    #region Return
    public void Return(T obj)
    {
        var leftNode = nextObjectLookup.First;
        var leftNodeValue = leftNode.Value;

        var rightNode = nextObjectLookup.Last;
        var rightNodeValue = rightNode.Value;

        while (leftNode != rightNode && rightNode.Next != leftNode)
        {
            if (leftNodeValue.objReference == obj)
            {
                leftNodeValue.inUse = false;

                nextObjectLookup.Remove(leftNode);
                nextObjectLookup.AddFirst(leftNode);
                break;
            }
            else if (rightNodeValue.objReference == obj)
            {
                rightNodeValue.inUse = false;

                nextObjectLookup.Remove(rightNode);
                nextObjectLookup.AddFirst(rightNode);
                break;
            }

            leftNode = leftNode.Next;
            leftNodeValue = leftNode.Value;

            rightNode = rightNode.Previous;
            rightNodeValue = rightNode.Value;
        }

        onReturnedToPool?.Invoke(obj);
    }
    #endregion

    #region IDisposable Implementation
    public void Clear()
    {
        for (int i = 0; i < pooledObjects.Count; ++i)
        {
            onObjectDestroyed?.Invoke(pooledObjects[i]);
        }

        pooledObjects.Clear();
        nextObjectLookup.Clear();
    }
    #endregion

    #region IEnumerator Implementation
    public IEnumerator GetEnumerator()
    {
        return pooledObjects.GetEnumerator();
    }
    #endregion
}
