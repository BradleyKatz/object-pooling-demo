using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Jobs;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

using Random = Unity.Mathematics.Random;

public class WanderSystem : MonoBehaviour
{
    [Header("System References")]
    [SerializeField] private EntitySpawner spawner;
    [SerializeField] private SpawnZone spawnZone;

    [Space]

    [Header("Wander Properties")]
    [SerializeField] private float wanderSpeed;
    [SerializeField] private float rotationSpeed;

    private float3 wanderZoneMin;
    private float3 wanderZoneMax;
    private Random rng;

    private JobHandle movementJobHandle;
    private TransformAccessArray transformsAccess;

    private void Awake()
    {
        rng = new Random((uint)System.DateTime.Now.Ticks);
        spawner.OnEntityCountChanged.AddListener(UpdateTransformAccessArray);

#if UNITY_EDITOR
        UnityEditor.EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
#endif
    }

#if UNITY_EDITOR
    private void OnPlayModeStateChanged(UnityEditor.PlayModeStateChange state)
    {
        switch (state)
        {
            case UnityEditor.PlayModeStateChange.ExitingPlayMode:
                if (transformsAccess.isCreated)
                    transformsAccess.Dispose();

                break;
        }
    }
#endif

    private void OnDestroy()
    {
        spawner.OnEntityCountChanged.RemoveListener(UpdateTransformAccessArray);

        if (transformsAccess.isCreated)
            transformsAccess.Dispose();
    }

    private void Update()
    {
        if (spawner.activeTransforms.Count > 0)
        {
            wanderZoneMin = new float3(spawnZone.Min);
            wanderZoneMax = new float3(spawnZone.Max);

            UpdateWanderJobs();
        }
    }

    private void UpdateTransformAccessArray()
    {
        if (transformsAccess.isCreated)
            transformsAccess.Dispose();

        transformsAccess = new TransformAccessArray(spawner.activeTransforms.ToArray());
    }

    private void UpdateWanderJobs()
    {
        movementJobHandle.Complete();

        if (transformsAccess.isCreated)
        {
            var movementJob = new WanderMovementJob();
            movementJob.rng = rng;
            movementJob.wanderSpeed = wanderSpeed;
            movementJob.rotationSpeed = rotationSpeed;
            movementJob.deltaTime = Time.deltaTime;
            movementJob.zoneMin = wanderZoneMin;
            movementJob.zoneMax = wanderZoneMax;

            movementJobHandle = movementJob.Schedule(transformsAccess);
            JobHandle.ScheduleBatchedJobs();
        }
    }

    #region Job Structs
    #region Wander Movement Job
    [BurstCompile]
    private struct WanderMovementJob : IJobParallelForTransform
    {
        [ReadOnly]
        public float wanderSpeed;

        [ReadOnly]
        public float rotationSpeed;

        [ReadOnly]
        public float deltaTime;

        [ReadOnly]
        public float3 zoneMin;

        [ReadOnly]
        public float3 zoneMax;

        [ReadOnly]
        public Random rng;

        public void Execute(int index, TransformAccess transform)
        {
            float3 forwardVector = transform.rotation * math.float3(0f, 0f, 1f);

            float3 newPos = math.float3(transform.position) + (forwardVector * wanderSpeed * deltaTime);
            newPos = math.clamp(newPos, zoneMin, zoneMax);
            transform.position = newPos;

            float theta = rng.NextFloat() * math.PI * 2f;
            transform.rotation = math.mul(math.normalize(transform.rotation), quaternion.AxisAngle(new float3(0f, 1f, 0f), rotationSpeed * math.cos(theta) * deltaTime));
        }
    }
    #endregion
    #endregion
}
